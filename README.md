# API meme-me

Meme-me, le magnifique jeu où l'on peut juger les performances de cringes des autres joueurs

Steps to run this project:
## Pré-requis : python 3.0

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file if needed
3. Run migrations with `npm run migration:run` if needed
3. Run `npm start` command

# Erreur

If you have an error with gyp :

1. Run `npm update`
2. Run `npm i`

# Routes

## Reporting

POST URL/reportings

{ \
&emsp;"user_reported_id": string, \
&emsp;"user_report_id": string, \
} 

GET URL/reportings

GET URL/reportings/{id}

PATCH URL/reportings/{id}

{ \
&emsp;"user_reported_id": string, \
&emsp;"user_report_id": string, \
} 

DELETE URL/reportings/{id}
