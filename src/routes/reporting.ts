import { Router } from "express";
import ReportingController from "../controllers/ReportingController";

const router = Router();

router.get("/", ReportingController.listAll);

router.get(
    "/:id([0-9]+)",
    ReportingController.getOneById
);

router.post("/", ReportingController.newReporting);

router.patch(
    "/:id([0-9]+)",
    ReportingController.editReporting
);

router.delete(
    "/:id([0-9]+)",
    ReportingController.deleteReporting
);

router.get(
    "/findByUserReport/:id",
    ReportingController.findReportsByIdUserReport
);

router.get(
    "/findByUserReported/:id",
    ReportingController.findReportsByIdUserReported
);

export default router;