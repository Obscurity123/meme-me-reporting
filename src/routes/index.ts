import { Router, Request, Response } from "express";
import reporting from "./reporting";

const routes = Router();

routes.use("/reportings", reporting);

export default routes;