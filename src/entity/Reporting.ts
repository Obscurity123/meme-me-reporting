import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    Unique
} from "typeorm";

@Entity()
@Unique("reported_report", ["user_reported_id", "user_report_id"])
export class Reporting {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_reported_id: string;

    @Column()
    user_report_id: string;

    @Column()
    commentaire: string;
    
    @Column()
    @CreateDateColumn()
    createdAt: Date;

    @Column()
    @UpdateDateColumn()
    updatedAt: Date;

}