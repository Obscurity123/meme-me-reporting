import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Reporting } from "../entity/Reporting";

class ReportingController {

    static listAll = async (req: Request, res: Response) => {
        //Get reporting from database
        const reportingRepository = getRepository(Reporting);
        const reportings = await reportingRepository.find({
            select: ["id", "user_reported_id", "user_report_id", "commentaire", "createdAt", 'updatedAt']
        });

        //Send the reportings object
        res.send({"reportings": reportings});
    };

    static getOneById = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: number = parseInt(req.params.id);

        //Get the Reporting from database
        const reportingRepository = getRepository(Reporting);
        try {
            const reporting = await reportingRepository.findOneOrFail(id, {
                select: ["id", "user_reported_id", "user_report_id", "commentaire", "createdAt", 'updatedAt']
            });
            res.status(200).send({"reporting": reporting});
        } catch (error) {
            res.status(404).send({"error":error.message});
        }
    };

    static newReporting = async (req: Request, res: Response) => {
        //Get parameters from the body
        let { user_reported_id, user_report_id, commentaire } = req.body;

        let reporting = new Reporting();

        reporting.user_reported_id = user_reported_id;
        reporting.user_report_id = user_report_id;
        reporting.commentaire = commentaire;

        //Validade if the parameters are ok
        const errors = await validate(Reporting);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to save. If fails, the username is already in use
        const reportingRepository = getRepository(Reporting);
        try {
            await reportingRepository.save(reporting);
            res.status(201).send({"reporting":reporting})
        } catch (error) {
            res.status(409).send({"error": error.message});
            return;
        }

    };

    static editReporting = async (req: Request, res: Response) => {
        
        //Get the ID from the url
        const id = req.params.id;
        //Get values from the body
        let { user_reported_id, user_report_id, commentaire  } = req.body;

        //Try to find Reporting on database
        const reportingRepository = getRepository(Reporting);

        let reporting;
        try {
            reporting = await reportingRepository.findOneOrFail(id);
            await reportingRepository.save({...reporting, ...req.body });
            //After all send a 204 (no content, but accepted) response
            res.status(204).send();
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send({"error": error.message});
            return;
        }

    };

    static deleteReporting = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id = req.params.id;

        const reportingRepository = getRepository(Reporting);
        let reporting: Reporting;
        try {
            reporting = await reportingRepository.findOneOrFail(id);
            reportingRepository.delete(id);
            //After all send a 204 (no content, but accepted) response
            res.status(200).send("Reporting with id : "+id+" deleted");
        } catch (error) {
            res.status(404).send({"error":error.message});
            return;
        }
    };

    static findReportsByIdUserReport = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: string = req.params.id;

        //Get the Reporting from database
        const reportingRepository = getRepository(Reporting);
        try {
            const reportings = await reportingRepository.find({
                where: {user_report_id: id}    
            });
            res.status(200).send({"reportings": reportings});
        } catch (error) {
            res.status(404).send({"error":error.message});
        }
    };

    static findReportsByIdUserReported = async (req: Request, res: Response) => {
        //Get the ID from the url
        const id: string = req.params.id;

        //Get the Reporting from database
        const reportingRepository = getRepository(Reporting);
        try {
            const reportings = await reportingRepository.find({
                where: {user_reported_id: id}    
            });
            res.status(200).send({"reportings": reportings});
        } catch (error) {
            res.status(404).send({"error":error.message});
        }
    };
};

export default ReportingController;